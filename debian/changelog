node-eslint-plugin-es (4.1.0~ds1-5+apertis1) apertis; urgency=medium

  * Sync updates from debian/bookworm
  * debian: Enable pandoc documentation.

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Wed, 19 Apr 2023 20:35:47 +0530

node-eslint-plugin-es (4.1.0~ds1-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + node-eslint-plugin-es: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 28 Nov 2022 17:40:48 +0000

node-eslint-plugin-es (4.1.0~ds1-4) unstable; urgency=medium

  * update git-buildpackage config: add usage comment
  * generate documentation with cmark-gfm (not pandoc);
    build-depend on cmark-gfm (not pandoc)
  * add patch 2002
    to avoid eslint prettier hints unsupported in Debian;
    tighten build-dependency on node-mysticatea-eslint-plugin;
    closes: bug#1006058, thakns to Lucas Nussbaum
  * declare compliance with Debian Policy 4.6.0
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * simplify source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Feb 2022 18:35:35 +0100

node-eslint-plugin-es (4.1.0~ds1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Sep 2021 16:25:18 +0100

node-eslint-plugin-es (4.1.0~ds1-2apertis1) apertis; urgency=medium

  * debian: Disable pandoc documentation. Avoid pulling in the whole cluster
    of package dependencies.

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Mon, 03 May 2021 08:37:18 -0300

node-eslint-plugin-es (4.1.0~ds1-2apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:27:02 +0200

node-eslint-plugin-es (4.1.0~ds1-2) unstable; urgency=medium

  * fix have autopkgtest depend on nodejs
  * add patch 1002
    to comply with @mysticatea/eslint-plugin/meta-property-ordering

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Dec 2020 05:10:13 +0100

node-eslint-plugin-es (4.1.0~ds1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * copyright:
    + avoid more pre-generated code when repackaging upstream source
  * drop patch cherry-picked upstream since applied
  * add patch 1001 to tidy code making it eslint clean
  * update patch 2001

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 15 Dec 2020 01:05:21 +0100

node-eslint-plugin-es (1.4.1~ds1-2) unstable; urgency=medium

  * support DEB_BUILD_OPTIONS=terse

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Dec 2020 12:00:36 +0100

node-eslint-plugin-es (1.4.1~ds1-1) experimental; urgency=low

  * initial release;
    closes: Bug#976841

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Dec 2020 15:40:23 +0100
